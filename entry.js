import { save } from "./api";

export default class Entry {
  constructor() {
    this.fields = {};
  }

  async fill(fields) {
    await Promise.resolve();
    Object.assign(this.fields, fields);
  }

  async save(fields) {
    await this.fill(fields);
    return save(this.fields);
  }
}
